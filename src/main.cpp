#include <iostream>
#include <libpq-fe.h>
#include <cstring>
#include <iomanip>

// Déclaration des fonctions
void ligne_separation(int nbcolonnes, int maxchamp, char ligne);

// Programme principale
int main(){
  PGPing code_retour_ping;
  PGconn *connexion;
  ConnStatusType status;
  char *host, *user, *mdp, *bdd, *port;
  int ssl, version_protocole, version_serveur, version_bibliotheque;
  std::string retour_ssl;
  const char *encodage;
  PGresult *resultat;
  code_retour_ping = PQping("host=postgresql.bts-malraux72.net port=5432"); // Vérification de l'accessibilité du serveur postgresql
  connexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=m.baglan connect_timeout=10"); // Connexion au serveur postgresql
  status = PQstatus(connexion); // Vérification de la connexion au serveur (connexion réussie ou échec)
  host = PQhost(connexion); // Retourne le nom d'hôte du serveur
  user = PQuser(connexion); // Retourne le nom d'utilisateur utilisé pour la connexion
  mdp = PQpass(connexion); // Retourne le mot de passe de l'utilisateur utilisé pour la connexion
  bdd = PQdb(connexion); // Retourne le nom de la base de données de la connexion
  port = PQport(connexion); // Retourne le port TCP utilisé pour la connexion
  ssl = PQsslInUse(connexion); // Retourne true (1) si la connexion utilise SSL, false (0) dans le cas contraire
  encodage = PQparameterStatus(connexion, "server_encoding"); // Retourne le type d'encodage utilisé pour la connexion
  version_protocole = PQprotocolVersion(connexion); // Retourne un entier qui représente la version du protocole
  version_serveur = PQserverVersion(connexion); // Retourne un entier qui représente la version du serveur
  version_bibliotheque = PQlibVersion(); // Retourne un entier qui représente la version de la bibliothèque ’libpq’

  if(code_retour_ping == PQPING_OK) // Si le serveur est accessible
  {
    std::cout << "La connexion au serveur de base de données ’" << host << "’ a été établie avec les paramètres suivants : " << std::endl;

    if(status == CONNECTION_OK) // Si la connexion est OK
    {
      // Affichage des informations de connexion
      std::cout << " * utilisateur : " << user << std::endl;
      std::cout << " * mot de passe : ";
      for (int i = 0; i < std::strlen(mdp); ++i)
      {
        std::cout << "*";
      }
      std::cout << std::endl;
      std::cout << " * base de données : " << bdd << std::endl;
      std::cout << " * port TCP : " << port << std::endl;
      std::cout << " * chiffrement SSL : " << (ssl?"true":"false") << std::endl; // condition ternaire
      std::cout << " * encodage : " << encodage << std::endl;
      std::cout << " * version du protocole : " << version_protocole << std::endl;
      std::cout << " * version du serveur : " << version_serveur << std::endl;
      std::cout << " * version de la bibliothèque ’libpq’ du client : " << version_bibliotheque << std::endl;

      // Requête envoyée au serveur
      resultat = PQexec(connexion, "SELECT \"Animal\".id, \"Animal\".nom, \"Animal\".sexe, \"Animal\".date_naissance, \"Animal\".commentaires, \"Race\".nom, \"Race\".description FROM si6.\"Animal\" INNER JOIN si6.\"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura'");

      // Renvoie l'état du résultat de la requête
      ExecStatusType status;
      status = PQresultStatus(resultat);

      // Validation de l'exécution de la requête
       if(status == PGRES_TUPLES_OK)
       {
         // std::cout << "Fin avec succès d'une commande renvoyant des données (telle que SELECT ou SHOW)" << std::endl;

         int nblignes = PQntuples(resultat);
         int nbcolonnes = PQnfields(resultat);
         const char separateur = '|';
         const char ligne = '-';
         const char point = '.';
         int maxchamp = 0;
         int largeur = 0;

         // Normalisation de la largeur des champs à la longueur du nom de champ le plus long
         for(int i = 0; i < nbcolonnes; ++i)
         {
           char *nomcolonnes = PQfname(resultat, i);

           if(std::strlen(nomcolonnes) > maxchamp)
           {
             maxchamp = std::strlen(nomcolonnes);
           }
         }

         // Ligne de séparation entre les paramètres de connexion au serveur et l'en-tête
         ligne_separation(nbcolonnes, maxchamp, ligne);

         // Affichage de l'en-tête présentant le nom de chacun des champs issus de la requête
         for(int e = 0; e < nbcolonnes; ++e)
         {
           std::cout << separateur << " " << std::left << std::setw(maxchamp) << PQfname(resultat, e) << " ";
         }
         std::cout << " " << separateur << std::endl;

         // Ligne de séparation entre l'en-tête et les données
         ligne_separation(nbcolonnes, maxchamp, ligne);

         // Affichage des données concernant les résultats obtenus
         for(int l = 0; l < nblignes; ++l)
         {
           for(int c = 0; c < nbcolonnes; ++c)
           {
             // Tronquage de la chaîne de caractères du champ de données à la largeur du champ de l'en-tête
             char *valeurs = PQgetvalue(resultat, l, c);
             largeur = std::strlen(valeurs);
             if(largeur > maxchamp){
               valeurs[maxchamp] = '\0';
               for(int t = maxchamp - 3; t < maxchamp; ++t){
                 valeurs[t] = point;
               }
             }
             std::cout << separateur << " " << std::left << std::setw(maxchamp) << valeurs << " ";
           }

           std::cout << " " << separateur << std::endl;
         }

         // Ligne de séparation entre les données et le nombre d'enregistrements retournés par la requête
         ligne_separation(nbcolonnes, maxchamp, ligne);

         std::cout << std::endl;
         std::cout << "L'exécution de la requête SQL a retourné " << nblignes << " enregistrements." << std::endl;
       }

       // Gestion des erreurs possibles
       else if(status == PGRES_EMPTY_QUERY)
       {
         std::cerr << "La chaîne envoyée au serveur était vide." << std::endl;
       }
       else if(status == PGRES_COMMAND_OK)
       {
         std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée." << std::endl;
       }
       else if(status == PGRES_COPY_OUT)
       {
         std::cout << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
       }
       else if(status == PGRES_COPY_IN)
       {
         std::cout << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
       }
       else if(status == PGRES_BAD_RESPONSE)
       {
         std::cerr << "La réponse du serveur n'a pas été comprise." << std::endl;
       }
       else if(status == PGRES_NONFATAL_ERROR)
       {
         std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
       }
       else if(status == PGRES_FATAL_ERROR)
       {
         std::cerr << "Une erreur fatale est survenue." << std::endl;
       }
       else if(status == PGRES_COPY_BOTH)
       {
         std::cout << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur)." << std::endl;
       }
       else if(status == PGRES_SINGLE_TUPLE)
       {
         std::cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode ligne-à-ligne a été sélectionné pour cette requête." << std::endl;
       }

    }
    else // Sinon échec de la connexion au serveur
    {
      std::cerr << "Connexion échouée" << std::endl;
    }
  }
  else // Sinon le serveur est inaccessible
  {
    std::cerr << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité." << std::endl;
  }

  return 0;
}

// Fonctions

// Fonction pour tracer la ligne de séparation à utiliser dans le dessin du tableau de données
void ligne_separation(int nbcolonnes, int maxchamp, char ligne){
  for(int s = 0; s < nbcolonnes * (maxchamp + 3); ++s) // 3 correspond à la barre verticale pour séparer les champs + les deux espaces avant et après celle-ci : " | "
  {
    std::cout << ligne;
  }
  std::cout << ligne << ligne; // On rajoute deux tirets qui correspondent au dernier séparateur ainsi que l'espace qui le précède : " |"
  std::cout << std::endl;
}
